## Waykichain Weekly Status Report

* 技术部周报 
  * 个人周报
    * 发送邮箱： ```rdg @ waykichainhk dot com```
    * 每周五下班前完成发送
    * 发送核心内容
      * 本周完成工作（需要量化）
      * 下周工作规划（SMART）
      * 其它
 * 技术部周报汇总
   * 直接在本项目内编写，一月一文件方式保存汇总周报
   * 轮班人员名册和顺序
   
```javascript
   - CH
   - HJN
   - HWH
   - LCL
   - LZS
   - PP
   - SGL
   - SN
   - WHQ
   - WJQ
   - WSH
   - WSS
   - WWJ
   - XSH
   - XYH
   - YH
   - YKL
   - ZDQ

```
   * 轮班汇总开始日期：```2018-09-08```
     * 轮班汇总完成时间：```每周六晚上10点钟之前```
     * 汇总范围：
       * 技术部集体员工本周项目进展和下周规划
       * 注意信息安全，内部信息不对外披露
       * 最后发布由CTO审阅后定稿
